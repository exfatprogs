Source: exfatprogs
Section: otherosfs
Priority: optional
Maintainer: Sven Hoexter <hoexter@debian.org>
Build-Depends: debhelper-compat (= 13), pkgconf, xxd
Standards-Version: 4.7.2
Rules-Requires-Root: no
Homepage: https://github.com/exfatprogs/exfatprogs
Vcs-Git: https://salsa.debian.org/debian/exfatprogs.git
Vcs-Browser: https://salsa.debian.org/debian/exfatprogs

Package: exfatprogs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: exFAT file system utilities
 Tools to manage extended file allocation table filesystem.
 This package provides tools to create, check, dump and label
 the filesystem. It contains
  - mkfs.exfat to create an exFAT filesystem
  - fsck.exfat to check and repair an exFAT filesystem
  - tune.exfat to print and edit the volume label or serial
  - dump.exfat to show on-disk information of an exFAT filesystem
  - exfat2img to dump exFAT metadata
 The tools included in this package are the exfatprogs
 maintained by Samsung and LG engineers, who provided Linux exFAT
 support.
